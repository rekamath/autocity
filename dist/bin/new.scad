scale(v = [0.85, 0.85, 0.85]) {
  union() {
    scale(v = [0.15, 0.15, 0.15]) {
      union() {
        union() {
          translate(v = [0, 0, 75]) {
            cube(size = [200, 350, 150], center = true);
          }
          translate(v = [0, -148.75, 0]) {
            translate(v = [-72, 0, 0]) {
              rotate(a = 90, v = [0, 0, 1]) {
                translate(v = [0, 0, 75]) {
intersection() {
                  cube(size = [140, 40, 150], center = true);
                  rotate(a = 8, v = [0, 1, 0]) {
                    cube(size = [70, 20, 300], center = true);
                  }
                }
              }
            }
          }
          translate(v = [-36, 0, 0]) {
            rotate(a = 90, v = [0, 0, 1]) {
              translate(v = [0, 0, 75]) {
intersection() {
                cube(size = [140, 40, 150], center = true);
                rotate(a = 8, v = [0, 1, 0]) {
                  cube(size = [70, 20, 300], center = true);
                }
              }
            }
          }
        }
        translate(v = [0, 0, 0]) {
          rotate(a = 90, v = [0, 0, 1]) {
            translate(v = [0, 0, 75]) {
intersection() {
              cube(size = [140, 40, 150], center = true);
              rotate(a = 8, v = [0, 1, 0]) {
                cube(size = [70, 20, 300], center = true);
              }
            }
          }
        }
      }
      translate(v = [36, 0, 0]) {
        rotate(a = 90, v = [0, 0, 1]) {
          translate(v = [0, 0, 75]) {
intersection() {
            cube(size = [140, 40, 150], center = true);
            rotate(a = 8, v = [0, 1, 0]) {
              cube(size = [70, 20, 300], center = true);
            }
          }
        }
      }
    }
    translate(v = [72, 0, 0]) {
      rotate(a = 90, v = [0, 0, 1]) {
        translate(v = [0, 0, 75]) {
intersection() {
          cube(size = [140, 40, 150], center = true);
          rotate(a = 8, v = [0, 1, 0]) {
            cube(size = [70, 20, 300], center = true);
          }
        }
      }
    }
  }
}

rotate(a = 180, v = [0, 0, 1]) {
  translate(v = [0, -148.75, 0]) {
    translate(v = [-72, 0, 0]) {
      rotate(a = 90, v = [0, 0, 1]) {
        translate(v = [0, 0, 75]) {
intersection() {
          cube(size = [140, 40, 150], center = true);
          rotate(a = 8, v = [0, 1, 0]) {
            cube(size = [70, 20, 300], center = true);
          }
        }
      }
    }
  }
  translate(v = [-36, 0, 0]) {
    rotate(a = 90, v = [0, 0, 1]) {
      translate(v = [0, 0, 75]) {
intersection() {
        cube(size = [140, 40, 150], center = true);
        rotate(a = 8, v = [0, 1, 0]) {
          cube(size = [70, 20, 300], center = true);
        }
      }
    }
  }
}

translate(v = [0, 0, 0]) {
  rotate(a = 90, v = [0, 0, 1]) {
    translate(v = [0, 0, 75]) {
intersection() {
      cube(size = [140, 40, 150], center = true);
      rotate(a = 8, v = [0, 1, 0]) {
        cube(size = [70, 20, 300], center = true);
      }
    }
  }
}

}

translate(v = [36, 0, 0]) {
  rotate(a = 90, v = [0, 0, 1]) {
    translate(v = [0, 0, 75]) {
intersection() {
      cube(size = [140, 40, 150], center = true);
      rotate(a = 8, v = [0, 1, 0]) {
        cube(size = [70, 20, 300], center = true);
      }
    }
  }
}

}

translate(v = [72, 0, 0]) {
  rotate(a = 90, v = [0, 0, 1]) {
    translate(v = [0, 0, 75]) {
intersection() {
      cube(size = [140, 40, 150], center = true);
      rotate(a = 8, v = [0, 1, 0]) {
        cube(size = [70, 20, 300], center = true);
      }
    }
  }
}

}

}

}

rotate(a = 90, v = [0, 0, 1]) {
  translate(v = [0, -85, 0]) {
    translate(v = [-126, 0, 0]) {
      rotate(a = 90, v = [0, 0, 1]) {
        translate(v = [0, 0, 75]) {
intersection() {
          cube(size = [80, 35, 150], center = true);
          rotate(a = 8, v = [0, 1, 0]) {
            cube(size = [40, 17.5, 300], center = true);
          }
        }
      }
    }
  }
  translate(v = [-63, 0, 0]) {
    rotate(a = 90, v = [0, 0, 1]) {
      translate(v = [0, 0, 75]) {
intersection() {
        cube(size = [80, 35, 150], center = true);
        rotate(a = 8, v = [0, 1, 0]) {
          cube(size = [40, 17.5, 300], center = true);
        }
      }
    }
  }
}

translate(v = [0, 0, 0]) {
  rotate(a = 90, v = [0, 0, 1]) {
    translate(v = [0, 0, 75]) {
intersection() {
      cube(size = [80, 35, 150], center = true);
      rotate(a = 8, v = [0, 1, 0]) {
        cube(size = [40, 17.5, 300], center = true);
      }
    }
  }
}

}

translate(v = [63, 0, 0]) {
  rotate(a = 90, v = [0, 0, 1]) {
    translate(v = [0, 0, 75]) {
intersection() {
      cube(size = [80, 35, 150], center = true);
      rotate(a = 8, v = [0, 1, 0]) {
        cube(size = [40, 17.5, 300], center = true);
      }
    }
  }
}

}

translate(v = [126, 0, 0]) {
  rotate(a = 90, v = [0, 0, 1]) {
    translate(v = [0, 0, 75]) {
intersection() {
      cube(size = [80, 35, 150], center = true);
      rotate(a = 8, v = [0, 1, 0]) {
        cube(size = [40, 17.5, 300], center = true);
      }
    }
  }
}

}

}

rotate(a = 180, v = [0, 0, 1]) {
  translate(v = [0, -85, 0]) {
    translate(v = [-126, 0, 0]) {
      rotate(a = 90, v = [0, 0, 1]) {
        translate(v = [0, 0, 75]) {
intersection() {
          cube(size = [80, 35, 150], center = true);
          rotate(a = 8, v = [0, 1, 0]) {
            cube(size = [40, 17.5, 300], center = true);
          }
        }
      }
    }
  }
  translate(v = [-63, 0, 0]) {
    rotate(a = 90, v = [0, 0, 1]) {
      translate(v = [0, 0, 75]) {
intersection() {
        cube(size = [80, 35, 150], center = true);
        rotate(a = 8, v = [0, 1, 0]) {
          cube(size = [40, 17.5, 300], center = true);
        }
      }
    }
  }
}

translate(v = [0, 0, 0]) {
  rotate(a = 90, v = [0, 0, 1]) {
    translate(v = [0, 0, 75]) {
intersection() {
      cube(size = [80, 35, 150], center = true);
      rotate(a = 8, v = [0, 1, 0]) {
        cube(size = [40, 17.5, 300], center = true);
      }
    }
  }
}

}

translate(v = [63, 0, 0]) {
  rotate(a = 90, v = [0, 0, 1]) {
    translate(v = [0, 0, 75]) {
intersection() {
      cube(size = [80, 35, 150], center = true);
      rotate(a = 8, v = [0, 1, 0]) {
        cube(size = [40, 17.5, 300], center = true);
      }
    }
  }
}

}

translate(v = [126, 0, 0]) {
  rotate(a = 90, v = [0, 0, 1]) {
    translate(v = [0, 0, 75]) {
intersection() {
      cube(size = [80, 35, 150], center = true);
      rotate(a = 8, v = [0, 1, 0]) {
        cube(size = [40, 17.5, 300], center = true);
      }
    }
  }
}

}

}

}

}

}

}

translate(v = [0, 0, 150]) {
  translate(v = [0, 0, 0]) {
    rotate(a = 90, v = [0, 0, 1]) {
      difference() {
        translate(v = [0, 0, 61.25]) {
          cube(size = [339.5, 194, 122.5], center = true);
        }
        translate(v = [0, 0, 52.5]) {
          union() {
            translate(v = [-105, 0, 0]) {
              rotate(a = 90, v = [0, 0, 1]) {
                translate(v = [-100, 0, 25]) {
                  union() {
intersection() {
                    translate(v = [0, 18.75, 0]) {
                      rotate(a = 90, v = [0, 1, 0]) {
                        cylinder(h = 200, r1 = 31.25, r2 = 31.25);
                      }
                    }
                    translate(v = [0, -18.75, 0]) {
                      rotate(a = 90, v = [0, 1, 0]) {
                        cylinder(h = 200, r1 = 31.25, r2 = 31.25);
                      }
                    }
                  }
                  translate(v = [100, 0, -12.5]) {
                    cube(size = [200, 25, 25], center = true);
                  }
                }
              }
            }
          }
          translate(v = [0, 0, 0]) {
            rotate(a = 90, v = [0, 0, 1]) {
              translate(v = [-100, 0, 25]) {
                union() {
intersection() {
                  translate(v = [0, 18.75, 0]) {
                    rotate(a = 90, v = [0, 1, 0]) {
                      cylinder(h = 200, r1 = 31.25, r2 = 31.25);
                    }
                  }
                  translate(v = [0, -18.75, 0]) {
                    rotate(a = 90, v = [0, 1, 0]) {
                      cylinder(h = 200, r1 = 31.25, r2 = 31.25);
                    }
                  }
                }
                translate(v = [100, 0, -12.5]) {
                  cube(size = [200, 25, 25], center = true);
                }
              }
            }
          }
        }
        translate(v = [105, 0, 0]) {
          rotate(a = 90, v = [0, 0, 1]) {
            translate(v = [-100, 0, 25]) {
              union() {
intersection() {
                translate(v = [0, 18.75, 0]) {
                  rotate(a = 90, v = [0, 1, 0]) {
                    cylinder(h = 200, r1 = 31.25, r2 = 31.25);
                  }
                }
                translate(v = [0, -18.75, 0]) {
                  rotate(a = 90, v = [0, 1, 0]) {
                    cylinder(h = 200, r1 = 31.25, r2 = 31.25);
                  }
                }
              }
              translate(v = [100, 0, -12.5]) {
                cube(size = [200, 25, 25], center = true);
              }
            }
          }
        }
      }
      rotate(a = 90, v = [0, 0, 1]) {
        translate(v = [-41.25, 0, 0]) {
          rotate(a = 90, v = [0, 0, 1]) {
            translate(v = [-175, 0, 25]) {
              union() {
intersection() {
                translate(v = [0, 18.75, 0]) {
                  rotate(a = 90, v = [0, 1, 0]) {
                    cylinder(h = 350, r1 = 31.25, r2 = 31.25);
                  }
                }
                translate(v = [0, -18.75, 0]) {
                  rotate(a = 90, v = [0, 1, 0]) {
                    cylinder(h = 350, r1 = 31.25, r2 = 31.25);
                  }
                }
              }
              translate(v = [175, 0, -12.5]) {
                cube(size = [350, 25, 25], center = true);
              }
            }
          }
        }
      }
      translate(v = [41.25, 0, 0]) {
        rotate(a = 90, v = [0, 0, 1]) {
          translate(v = [-175, 0, 25]) {
            union() {
intersection() {
              translate(v = [0, 18.75, 0]) {
                rotate(a = 90, v = [0, 1, 0]) {
                  cylinder(h = 350, r1 = 31.25, r2 = 31.25);
                }
              }
              translate(v = [0, -18.75, 0]) {
                rotate(a = 90, v = [0, 1, 0]) {
                  cylinder(h = 350, r1 = 31.25, r2 = 31.25);
                }
              }
            }
            translate(v = [175, 0, -12.5]) {
              cube(size = [350, 25, 25], center = true);
            }
          }
        }
      }
    }
  }
}

}

}

translate(v = [0, 0, 122.5]) {
  union() {
intersection() {
    scale(v = [0.583333, 1, 1]) {
      difference() {
        rotate(a = 45, v = [0, 1, 0]) {
          cube(size = [424.264, 200, 424.264], center = true);
        }
        translate(v = [0, 0, -424.264]) {
          cube(size = [848.528, 400, 848.528], center = true);
        }
      }
    }
    rotate(a = 90, v = [0, 0, 1]) {
      scale(v = [1, 1, 1]) {
        difference() {
          rotate(a = 45, v = [0, 1, 0]) {
            cube(size = [141.421, 350, 141.421], center = true);
          }
          translate(v = [0, 0, -141.421]) {
            cube(size = [282.843, 700, 282.843], center = true);
          }
        }
      }
    }
  }
  difference() {
    translate(v = [0, 0, 17.5]) {
      difference() {
        cube(size = [350, 200, 35], center = true);
        cube(size = [309.689, 159.689, 55.1556], center = true);
      }
    }
    translate(v = [0, 0, 20.75]) {
/* n = 8 */
      translate(v = [0, 0, 7.5]) {
        translate(v = [-144.307, 0, 0]) {
          rotate(a = 90, v = [0, 0, 1]) {
            cube(size = [220, 10.0778, 15], center = true);
          }
        }
        translate(v = [-103.076, 0, 0]) {
          rotate(a = 90, v = [0, 0, 1]) {
            cube(size = [220, 10.0778, 15], center = true);
          }
        }
        translate(v = [-61.8458, 0, 0]) {
          rotate(a = 90, v = [0, 0, 1]) {
            cube(size = [220, 10.0778, 15], center = true);
          }
        }
        translate(v = [-20.6153, 0, 0]) {
          rotate(a = 90, v = [0, 0, 1]) {
            cube(size = [220, 10.0778, 15], center = true);
          }
        }
        translate(v = [20.6153, 0, 0]) {
          rotate(a = 90, v = [0, 0, 1]) {
            cube(size = [220, 10.0778, 15], center = true);
          }
        }
        translate(v = [61.8458, 0, 0]) {
          rotate(a = 90, v = [0, 0, 1]) {
            cube(size = [220, 10.0778, 15], center = true);
          }
        }
        translate(v = [103.076, 0, 0]) {
          rotate(a = 90, v = [0, 0, 1]) {
            cube(size = [220, 10.0778, 15], center = true);
          }
        }
        translate(v = [144.307, 0, 0]) {
          rotate(a = 90, v = [0, 0, 1]) {
            cube(size = [220, 10.0778, 15], center = true);
          }
        }
      }
      rotate(a = 90, v = [0, 0, 1]) {
/* n = 4 */
        translate(v = [0, 0, 7.5]) {
          translate(v = [-67.4416, 0, 0]) {
            rotate(a = 90, v = [0, 0, 1]) {
              cube(size = [385, 10.0778, 15], center = true);
            }
          }
          translate(v = [-22.4805, 0, 0]) {
            rotate(a = 90, v = [0, 0, 1]) {
              cube(size = [385, 10.0778, 15], center = true);
            }
          }
          translate(v = [22.4805, 0, 0]) {
            rotate(a = 90, v = [0, 0, 1]) {
              cube(size = [385, 10.0778, 15], center = true);
            }
          }
          translate(v = [67.4416, 0, 0]) {
            rotate(a = 90, v = [0, 0, 1]) {
              cube(size = [385, 10.0778, 15], center = true);
            }
          }
        }
      }
    }
  }
}

}

}

}

}

}

/* 58.319190, -14.030888, angle = 103 */
translate(v = [58.9994, -9.71314, 0]) {
  rotate(a = 103.528, v = [0, 0, 1]) {
    cube(size = [59.9833, 5.2, 28.4523], center = false);
    translate(v = [0, 0, 23.2523]) {
      cube(size = [7.8, 5.2, 10.4], center = false);
    }
    translate(v = [13, 0, 23.2523]) {
      cube(size = [7.8, 5.2, 10.4], center = false);
    }
    translate(v = [26, 0, 23.2523]) {
      cube(size = [7.8, 5.2, 10.4], center = false);
    }
    translate(v = [39, 0, 23.2523]) {
      cube(size = [7.8, 5.2, 10.4], center = false);
    }
    translate(v = [52, 0, 23.2523]) {
      cube(size = [7.8, 5.2, 10.4], center = false);
    }
  }
}

translate(v = [58.9994, -9.71314, 0]) {
  difference() {
    union() {
      difference() {
        cylinder(h = 41.4869, r1 = 11.4892, r2 = 9.19132, $fa = 5);
        translate(v = [0, 0, -12.4461]) {
          cylinder(h = 33.1895, r1 = 8.04241, r2 = 6.43392, $fa = 5);
        }
        translate(v = [0, 0, 18.6691]) {
          cylinder(h = 14.5204, r1 = 6.89349, r2 = 0.919132, $fa = 5);
        }
      }
      translate(v = [0, 0, 34.5934]) {
        cylinder(h = 6.89349, r1 = 9.30621, r2 = 13.787, $fa = 5);
        translate(v = [0, 0, 6.89349]) {
          difference() {
            cylinder(h = 6.89349, r1 = 13.787, r2 = 13.787, $fa = 5);
            cylinder(h = 8.61686, r1 = 11.4892, r2 = 11.4892, $fa = 5);
            translate(v = [0, 0, 8.04241]) {
              rotate(a = 22.5, v = [0, 0, 1]) {
                union() {
                  scale(v = [0.1, 0.1, 0.1]) {
                    translate(v = [-17.2337, -344.675, -46.2438]) {
                      union() {
                        cube(size = [34.4675, 689.349, 75.2539], center = false);
                        translate(v = [17.2337, 689.349, 75.2539]) {
                          rotate(a = 90, v = [1, 0, 0]) {
                            cylinder(h = 689.349, r1 = 17.2337, r2 = 17.2337);
                          }
                        }
                      }
                    }
                  }
                  rotate(a = 90, v = [0, 0, 1]) {
                    scale(v = [0.1, 0.1, 0.1]) {
                      translate(v = [-17.2337, -344.675, -46.2438]) {
                        union() {
                          cube(size = [34.4675, 689.349, 75.2539], center = false);
                          translate(v = [17.2337, 689.349, 75.2539]) {
                            rotate(a = 90, v = [1, 0, 0]) {
                              cylinder(h = 689.349, r1 = 17.2337, r2 = 17.2337);
                            }
                          }
                        }
                      }
                    }
                  }
                  rotate(a = 45, v = [0, 0, 1]) {
                    scale(v = [0.1, 0.1, 0.1]) {
                      translate(v = [-17.2337, -344.675, -46.2438]) {
                        union() {
                          cube(size = [34.4675, 689.349, 75.2539], center = false);
                          translate(v = [17.2337, 689.349, 75.2539]) {
                            rotate(a = 90, v = [1, 0, 0]) {
                              cylinder(h = 689.349, r1 = 17.2337, r2 = 17.2337);
                            }
                          }
                        }
                      }
                    }
                  }
                  rotate(a = 135, v = [0, 0, 1]) {
                    scale(v = [0.1, 0.1, 0.1]) {
                      translate(v = [-17.2337, -344.675, -46.2438]) {
                        union() {
                          cube(size = [34.4675, 689.349, 75.2539], center = false);
                          translate(v = [17.2337, 689.349, 75.2539]) {
                            rotate(a = 90, v = [1, 0, 0]) {
                              cylinder(h = 689.349, r1 = 17.2337, r2 = 17.2337);
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

/* 17.791170, -43.034951, angle = 157 */
translate(v = [44.9685, 48.606, 0]) {
  rotate(a = 157.539, v = [0, 0, 1]) {
    cube(size = [46.5675, 5.2, 31.1114], center = false);
    translate(v = [0, 0, 25.9114]) {
      cube(size = [7.8, 5.2, 10.4], center = false);
    }
    translate(v = [13, 0, 25.9114]) {
      cube(size = [7.8, 5.2, 10.4], center = false);
    }
    translate(v = [26, 0, 25.9114]) {
      cube(size = [7.8, 5.2, 10.4], center = false);
    }
    translate(v = [39, 0, 25.9114]) {
      cube(size = [7.8, 5.2, 10.4], center = false);
    }
  }
}

translate(v = [44.9685, 48.606, 0]) {
  difference() {
    union() {
      difference() {
        cylinder(h = 40.2147, r1 = 7.96722, r2 = 6.37378, $fa = 5);
        translate(v = [0, 0, -12.0644]) {
          cylinder(h = 32.1718, r1 = 5.57705, r2 = 4.46164, $fa = 5);
        }
        translate(v = [0, 0, 18.0966]) {
          cylinder(h = 14.0751, r1 = 4.78033, r2 = 0.637378, $fa = 5);
        }
      }
      translate(v = [0, 0, 35.4344]) {
        cylinder(h = 4.78033, r1 = 6.45345, r2 = 9.56066, $fa = 5);
        translate(v = [0, 0, 4.78033]) {
          cylinder(h = 4.78033, r1 = 9.56066, r2 = 9.56066, $fa = 5);
          translate(v = [0, 0, 4.78033]) {
            cylinder(h = 16.5869, r1 = 9.56066, r2 = 0, $fa = 5);
          }
        }
      }
    }
    translate(v = [0, 0, 21.4395]) {
      rotate(a = 249.472, v = [0, 0, 1]) {
        scale(v = [0.1, 0.1, 0.1]) {
          translate(v = [-10, -119.508, -25]) {
            union() {
              cube(size = [20, 239.017, 40], center = false);
              translate(v = [10, 239.017, 40]) {
                rotate(a = 90, v = [1, 0, 0]) {
                  cylinder(h = 239.017, r1 = 10, r2 = 10);
                }
              }
            }
          }
        }
      }
    }
    translate(v = [0, 0, 43.2147]) {
      rotate(a = 22.5, v = [0, 0, 1]) {
        union() {
          scale(v = [0.1, 0.1, 0.1]) {
            translate(v = [-11.9508, -239.017, -16.7312]) {
              union() {
                cube(size = [23.9017, 478.033, 21.5115], center = false);
                translate(v = [11.9508, 478.033, 21.5115]) {
                  rotate(a = 90, v = [1, 0, 0]) {
                    cylinder(h = 478.033, r1 = 11.9508, r2 = 11.9508);
                  }
                }
              }
            }
          }
          rotate(a = 90, v = [0, 0, 1]) {
            scale(v = [0.1, 0.1, 0.1]) {
              translate(v = [-11.9508, -239.017, -16.7312]) {
                union() {
                  cube(size = [23.9017, 478.033, 21.5115], center = false);
                  translate(v = [11.9508, 478.033, 21.5115]) {
                    rotate(a = 90, v = [1, 0, 0]) {
                      cylinder(h = 478.033, r1 = 11.9508, r2 = 11.9508);
                    }
                  }
                }
              }
            }
          }
          rotate(a = 45, v = [0, 0, 1]) {
            scale(v = [0.1, 0.1, 0.1]) {
              translate(v = [-11.9508, -239.017, -16.7312]) {
                union() {
                  cube(size = [23.9017, 478.033, 21.5115], center = false);
                  translate(v = [11.9508, 478.033, 21.5115]) {
                    rotate(a = 90, v = [1, 0, 0]) {
                      cylinder(h = 478.033, r1 = 11.9508, r2 = 11.9508);
                    }
                  }
                }
              }
            }
          }
          rotate(a = 135, v = [0, 0, 1]) {
            scale(v = [0.1, 0.1, 0.1]) {
              translate(v = [-11.9508, -239.017, -16.7312]) {
                union() {
                  cube(size = [23.9017, 478.033, 21.5115], center = false);
                  translate(v = [11.9508, 478.033, 21.5115]) {
                    rotate(a = 90, v = [1, 0, 0]) {
                      cylinder(h = 478.033, r1 = 11.9508, r2 = 11.9508);
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

/* -14.782303, -41.325451, angle = -160 */
translate(v = [1.93358, 66.3972, 0]) {
  rotate(a = -160.318, v = [0, 0, 1]) {
    cube(size = [43.8897, 5.2, 29.7586], center = false);
    translate(v = [0, 0, 24.5586]) {
      cube(size = [7.8, 5.2, 10.4], center = false);
    }
    translate(v = [13, 0, 24.5586]) {
      cube(size = [7.8, 5.2, 10.4], center = false);
    }
    translate(v = [26, 0, 24.5586]) {
      cube(size = [7.8, 5.2, 10.4], center = false);
    }
    translate(v = [39, 0, 24.5586]) {
      cube(size = [7.8, 5.2, 10.4], center = false);
    }
  }
}

translate(v = [1.93358, 66.3972, 0]) {
  difference() {
    union() {
      difference() {
        cylinder(h = 41.8763, r1 = 12.0929, r2 = 9.67434, $fa = 72);
        translate(v = [0, 0, -12.5629]) {
          cylinder(h = 33.5011, r1 = 8.46504, r2 = 6.77203, $fa = 72);
        }
        translate(v = [0, 0, 18.8443]) {
          cylinder(h = 14.6567, r1 = 7.25575, r2 = 0.967434, $fa = 72);
        }
      }
      translate(v = [0, 0, 34.6206]) {
        cylinder(h = 7.25575, r1 = 9.79526, r2 = 14.5115, $fa = 72);
        translate(v = [0, 0, 7.25575]) {
          difference() {
            cylinder(h = 7.25575, r1 = 14.5115, r2 = 14.5115, $fa = 72);
            cylinder(h = 9.06969, r1 = 12.0929, r2 = 12.0929, $fa = 72);
            translate(v = [0, 0, 8.46504]) {
              rotate(a = 22.5, v = [0, 0, 1]) {
                union() {
                  scale(v = [0.1, 0.1, 0.1]) {
                    translate(v = [-18.1394, -362.788, -48.674]) {
                      union() {
                        cube(size = [36.2788, 725.575, 79.2086], center = false);
                        translate(v = [18.1394, 725.575, 79.2086]) {
                          rotate(a = 90, v = [1, 0, 0]) {
                            cylinder(h = 725.575, r1 = 18.1394, r2 = 18.1394);
                          }
                        }
                      }
                    }
                  }
                  rotate(a = 90, v = [0, 0, 1]) {
                    scale(v = [0.1, 0.1, 0.1]) {
                      translate(v = [-18.1394, -362.788, -48.674]) {
                        union() {
                          cube(size = [36.2788, 725.575, 79.2086], center = false);
                          translate(v = [18.1394, 725.575, 79.2086]) {
                            rotate(a = 90, v = [1, 0, 0]) {
                              cylinder(h = 725.575, r1 = 18.1394, r2 = 18.1394);
                            }
                          }
                        }
                      }
                    }
                  }
                  rotate(a = 45, v = [0, 0, 1]) {
                    scale(v = [0.1, 0.1, 0.1]) {
                      translate(v = [-18.1394, -362.788, -48.674]) {
                        union() {
                          cube(size = [36.2788, 725.575, 79.2086], center = false);
                          translate(v = [18.1394, 725.575, 79.2086]) {
                            rotate(a = 90, v = [1, 0, 0]) {
                              cylinder(h = 725.575, r1 = 18.1394, r2 = 18.1394);
                            }
                          }
                        }
                      }
                    }
                  }
                  rotate(a = 135, v = [0, 0, 1]) {
                    scale(v = [0.1, 0.1, 0.1]) {
                      translate(v = [-18.1394, -362.788, -48.674]) {
                        union() {
                          cube(size = [36.2788, 725.575, 79.2086], center = false);
                          translate(v = [18.1394, 725.575, 79.2086]) {
                            rotate(a = 90, v = [1, 0, 0]) {
                              cylinder(h = 725.575, r1 = 18.1394, r2 = 18.1394);
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    translate(v = [0, 0, 29.4667]) {
      rotate(a = 147.039, v = [0, 0, 1]) {
        scale(v = [0.1, 0.1, 0.1]) {
          translate(v = [-10, -181.394, -25]) {
            union() {
              cube(size = [20, 362.788, 40], center = false);
              translate(v = [10, 362.788, 40]) {
                rotate(a = 90, v = [1, 0, 0]) {
                  cylinder(h = 362.788, r1 = 10, r2 = 10);
                }
              }
            }
          }
        }
      }
    }
    translate(v = [0, 0, 26.9185]) {
      rotate(a = 94.9299, v = [0, 0, 1]) {
        scale(v = [0.1, 0.1, 0.1]) {
          translate(v = [-10, -181.394, -25]) {
            union() {
              cube(size = [20, 362.788, 40], center = false);
              translate(v = [10, 362.788, 40]) {
                rotate(a = 90, v = [1, 0, 0]) {
                  cylinder(h = 362.788, r1 = 10, r2 = 10);
                }
              }
            }
          }
        }
      }
    }
  }
}

/* -48.667643, -34.499415, angle = -125 */
translate(v = [-39.3919, 51.6149, 0]) {
  rotate(a = -125.332, v = [0, 0, 1]) {
    cube(size = [59.6553, 5.2, 29.7972], center = false);
    translate(v = [0, 0, 24.5972]) {
      cube(size = [7.8, 5.2, 10.4], center = false);
    }
    translate(v = [13, 0, 24.5972]) {
      cube(size = [7.8, 5.2, 10.4], center = false);
    }
    translate(v = [26, 0, 24.5972]) {
      cube(size = [7.8, 5.2, 10.4], center = false);
    }
    translate(v = [39, 0, 24.5972]) {
      cube(size = [7.8, 5.2, 10.4], center = false);
    }
    translate(v = [52, 0, 24.5972]) {
      cube(size = [7.8, 5.2, 10.4], center = false);
    }
  }
}

translate(v = [-39.3919, 51.6149, 0]) {
  difference() {
    union() {
      difference() {
        cylinder(h = 39.1331, r1 = 11.6637, r2 = 9.331, $fa = 72);
        translate(v = [0, 0, -11.7399]) {
          cylinder(h = 31.3064, r1 = 8.16462, r2 = 6.5317, $fa = 72);
        }
        translate(v = [0, 0, 17.6099]) {
          cylinder(h = 13.6966, r1 = 6.99825, r2 = 0.9331, $fa = 72);
        }
      }
      translate(v = [0, 0, 32.1348]) {
        cylinder(h = 6.99825, r1 = 9.44764, r2 = 13.9965, $fa = 72);
        translate(v = [0, 0, 6.99825]) {
          difference() {
            cylinder(h = 6.99825, r1 = 13.9965, r2 = 13.9965, $fa = 72);
            cylinder(h = 8.74781, r1 = 11.6637, r2 = 11.6637, $fa = 72);
            translate(v = [0, 0, 8.16462]) {
              rotate(a = 22.5, v = [0, 0, 1]) {
                union() {
                  scale(v = [0.1, 0.1, 0.1]) {
                    translate(v = [-17.4956, -349.912, -46.9466]) {
                      union() {
                        cube(size = [34.9912, 699.825, 76.3976], center = false);
                        translate(v = [17.4956, 699.825, 76.3976]) {
                          rotate(a = 90, v = [1, 0, 0]) {
                            cylinder(h = 699.825, r1 = 17.4956, r2 = 17.4956);
                          }
                        }
                      }
                    }
                  }
                  rotate(a = 90, v = [0, 0, 1]) {
                    scale(v = [0.1, 0.1, 0.1]) {
                      translate(v = [-17.4956, -349.912, -46.9466]) {
                        union() {
                          cube(size = [34.9912, 699.825, 76.3976], center = false);
                          translate(v = [17.4956, 699.825, 76.3976]) {
                            rotate(a = 90, v = [1, 0, 0]) {
                              cylinder(h = 699.825, r1 = 17.4956, r2 = 17.4956);
                            }
                          }
                        }
                      }
                    }
                  }
                  rotate(a = 45, v = [0, 0, 1]) {
                    scale(v = [0.1, 0.1, 0.1]) {
                      translate(v = [-17.4956, -349.912, -46.9466]) {
                        union() {
                          cube(size = [34.9912, 699.825, 76.3976], center = false);
                          translate(v = [17.4956, 699.825, 76.3976]) {
                            rotate(a = 90, v = [1, 0, 0]) {
                              cylinder(h = 699.825, r1 = 17.4956, r2 = 17.4956);
                            }
                          }
                        }
                      }
                    }
                  }
                  rotate(a = 135, v = [0, 0, 1]) {
                    scale(v = [0.1, 0.1, 0.1]) {
                      translate(v = [-17.4956, -349.912, -46.9466]) {
                        union() {
                          cube(size = [34.9912, 699.825, 76.3976], center = false);
                          translate(v = [17.4956, 699.825, 76.3976]) {
                            rotate(a = 90, v = [1, 0, 0]) {
                              cylinder(h = 699.825, r1 = 17.4956, r2 = 17.4956);
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    translate(v = [0, 0, 22.6451]) {
      rotate(a = 23.8063, v = [0, 0, 1]) {
        scale(v = [0.1, 0.1, 0.1]) {
          translate(v = [-10, -174.956, -25]) {
            union() {
              cube(size = [20, 349.912, 40], center = false);
              translate(v = [10, 349.912, 40]) {
                rotate(a = 90, v = [1, 0, 0]) {
                  cylinder(h = 349.912, r1 = 10, r2 = 10);
                }
              }
            }
          }
        }
      }
    }
    translate(v = [0, 0, 22.1374]) {
      rotate(a = 313.024, v = [0, 0, 1]) {
        scale(v = [0.1, 0.1, 0.1]) {
          translate(v = [-10, -174.956, -25]) {
            union() {
              cube(size = [20, 349.912, 40], center = false);
              translate(v = [10, 349.912, 40]) {
                rotate(a = 90, v = [1, 0, 0]) {
                  cylinder(h = 349.912, r1 = 10, r2 = 10);
                }
              }
            }
          }
        }
      }
    }
  }
}

/* -49.281181, 20.095918, angle = -67 */
translate(v = [-73.8913, 2.94727, 0]) {
  rotate(a = -67.8153, v = [0, 0, 1]) {
    cube(size = [53.2211, 5.2, 30.7206], center = false);
    translate(v = [0, 0, 25.5206]) {
      cube(size = [7.8, 5.2, 10.4], center = false);
    }
    translate(v = [13, 0, 25.5206]) {
      cube(size = [7.8, 5.2, 10.4], center = false);
    }
    translate(v = [26, 0, 25.5206]) {
      cube(size = [7.8, 5.2, 10.4], center = false);
    }
    translate(v = [39, 0, 25.5206]) {
      cube(size = [7.8, 5.2, 10.4], center = false);
    }
    translate(v = [52, 0, 25.5206]) {
      cube(size = [7.8, 5.2, 10.4], center = false);
    }
  }
}

translate(v = [-73.8913, 2.94727, 0]) {
  difference() {
    union() {
      difference() {
        cylinder(h = 41.9165, r1 = 12.876, r2 = 10.3008, $fa = 5);
        translate(v = [0, 0, -12.575]) {
          cylinder(h = 33.5332, r1 = 9.0132, r2 = 7.21056, $fa = 5);
        }
        translate(v = [0, 0, 18.8624]) {
          cylinder(h = 14.6708, r1 = 7.7256, r2 = 1.03008, $fa = 5);
        }
      }
      translate(v = [0, 0, 34.1909]) {
        cylinder(h = 7.7256, r1 = 10.4296, r2 = 15.4512, $fa = 5);
        translate(v = [0, 0, 7.7256]) {
          difference() {
            cylinder(h = 7.7256, r1 = 15.4512, r2 = 15.4512, $fa = 5);
            cylinder(h = 9.657, r1 = 12.876, r2 = 12.876, $fa = 5);
            translate(v = [0, 0, 9.0132]) {
              rotate(a = 22.5, v = [0, 0, 1]) {
                union() {
                  scale(v = [0.1, 0.1, 0.1]) {
                    translate(v = [-19.314, -386.28, -51.8259]) {
                      union() {
                        cube(size = [38.628, 772.56, 84.3378], center = false);
                        translate(v = [19.314, 772.56, 84.3378]) {
                          rotate(a = 90, v = [1, 0, 0]) {
                            cylinder(h = 772.56, r1 = 19.314, r2 = 19.314);
                          }
                        }
                      }
                    }
                  }
                  rotate(a = 90, v = [0, 0, 1]) {
                    scale(v = [0.1, 0.1, 0.1]) {
                      translate(v = [-19.314, -386.28, -51.8259]) {
                        union() {
                          cube(size = [38.628, 772.56, 84.3378], center = false);
                          translate(v = [19.314, 772.56, 84.3378]) {
                            rotate(a = 90, v = [1, 0, 0]) {
                              cylinder(h = 772.56, r1 = 19.314, r2 = 19.314);
                            }
                          }
                        }
                      }
                    }
                  }
                  rotate(a = 45, v = [0, 0, 1]) {
                    scale(v = [0.1, 0.1, 0.1]) {
                      translate(v = [-19.314, -386.28, -51.8259]) {
                        union() {
                          cube(size = [38.628, 772.56, 84.3378], center = false);
                          translate(v = [19.314, 772.56, 84.3378]) {
                            rotate(a = 90, v = [1, 0, 0]) {
                              cylinder(h = 772.56, r1 = 19.314, r2 = 19.314);
                            }
                          }
                        }
                      }
                    }
                  }
                  rotate(a = 135, v = [0, 0, 1]) {
                    scale(v = [0.1, 0.1, 0.1]) {
                      translate(v = [-19.314, -386.28, -51.8259]) {
                        union() {
                          cube(size = [38.628, 772.56, 84.3378], center = false);
                          translate(v = [19.314, 772.56, 84.3378]) {
                            rotate(a = 90, v = [1, 0, 0]) {
                              cylinder(h = 772.56, r1 = 19.314, r2 = 19.314);
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    translate(v = [0, 0, 28.9858]) {
      rotate(a = 234.929, v = [0, 0, 1]) {
        scale(v = [0.1, 0.1, 0.1]) {
          translate(v = [-10, -193.14, -25]) {
            union() {
              cube(size = [20, 386.28, 40], center = false);
              translate(v = [10, 386.28, 40]) {
                rotate(a = 90, v = [1, 0, 0]) {
                  cylinder(h = 386.28, r1 = 10, r2 = 10);
                }
              }
            }
          }
        }
      }
    }
  }
}

/* -16.340513, 59.541287, angle = -15 */
translate(v = [-53.7954, -46.3339, 0]) {
  rotate(a = -15.3464, v = [0, 0, 1]) {
    cube(size = [61.7428, 5.2, 29.6351], center = false);
    translate(v = [0, 0, 24.4351]) {
      cube(size = [7.8, 5.2, 10.4], center = false);
    }
    translate(v = [13, 0, 24.4351]) {
      cube(size = [7.8, 5.2, 10.4], center = false);
    }
    translate(v = [26, 0, 24.4351]) {
      cube(size = [7.8, 5.2, 10.4], center = false);
    }
    translate(v = [39, 0, 24.4351]) {
      cube(size = [7.8, 5.2, 10.4], center = false);
    }
    translate(v = [52, 0, 24.4351]) {
      cube(size = [7.8, 5.2, 10.4], center = false);
    }
  }
}

translate(v = [-53.7954, -46.3339, 0]) {
  difference() {
    union() {
      difference() {
        cylinder(h = 37.125, r1 = 10.4, r2 = 8.32, $fn = 4);
        translate(v = [0, 0, -11.1375]) {
          cylinder(h = 29.7, r1 = 7.28, r2 = 5.824, $fn = 4);
        }
        translate(v = [0, 0, 16.7063]) {
          cylinder(h = 12.9937, r1 = 6.24, r2 = 0.832, $fn = 4);
        }
      }
      translate(v = [0, 0, 30.885]) {
        cylinder(h = 6.24, r1 = 8.424, r2 = 12.48, $fn = 4);
        translate(v = [0, 0, 6.24]) {
          difference() {
            cylinder(h = 6.24, r1 = 12.48, r2 = 12.48, $fn = 4);
            cylinder(h = 7.8, r1 = 10.4, r2 = 10.4, $fn = 4);
            translate(v = [0, 0, 7.28]) {
              rotate(a = 22.5, v = [0, 0, 1]) {
                union() {
                  scale(v = [0.1, 0.1, 0.1]) {
                    translate(v = [-15.6, -312, -41.86]) {
                      union() {
                        cube(size = [31.2, 624, 68.12], center = false);
                        translate(v = [15.6, 624, 68.12]) {
                          rotate(a = 90, v = [1, 0, 0]) {
                            cylinder(h = 624, r1 = 15.6, r2 = 15.6);
                          }
                        }
                      }
                    }
                  }
                  rotate(a = 90, v = [0, 0, 1]) {
                    scale(v = [0.1, 0.1, 0.1]) {
                      translate(v = [-15.6, -312, -41.86]) {
                        union() {
                          cube(size = [31.2, 624, 68.12], center = false);
                          translate(v = [15.6, 624, 68.12]) {
                            rotate(a = 90, v = [1, 0, 0]) {
                              cylinder(h = 624, r1 = 15.6, r2 = 15.6);
                            }
                          }
                        }
                      }
                    }
                  }
                  rotate(a = 45, v = [0, 0, 1]) {
                    scale(v = [0.1, 0.1, 0.1]) {
                      translate(v = [-15.6, -312, -41.86]) {
                        union() {
                          cube(size = [31.2, 624, 68.12], center = false);
                          translate(v = [15.6, 624, 68.12]) {
                            rotate(a = 90, v = [1, 0, 0]) {
                              cylinder(h = 624, r1 = 15.6, r2 = 15.6);
                            }
                          }
                        }
                      }
                    }
                  }
                  rotate(a = 135, v = [0, 0, 1]) {
                    scale(v = [0.1, 0.1, 0.1]) {
                      translate(v = [-15.6, -312, -41.86]) {
                        union() {
                          cube(size = [31.2, 624, 68.12], center = false);
                          translate(v = [15.6, 624, 68.12]) {
                            rotate(a = 90, v = [1, 0, 0]) {
                              cylinder(h = 624, r1 = 15.6, r2 = 15.6);
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    translate(v = [0, 0, 23.7273]) {
      rotate(a = 111.4, v = [0, 0, 1]) {
        scale(v = [0.1, 0.1, 0.1]) {
          translate(v = [-10, -156, -25]) {
            union() {
              cube(size = [20, 312, 40], center = false);
              translate(v = [10, 312, 40]) {
                rotate(a = 90, v = [1, 0, 0]) {
                  cylinder(h = 312, r1 = 10, r2 = 10);
                }
              }
            }
          }
        }
      }
    }
  }
}

/* 8.701713, 33.623650, angle = 14 */
translate(v = [5.74592, -62.6744, 0]) {
  rotate(a = 14.5097, v = [0, 0, 1]) {
    cube(size = [34.7314, 5.2, 28.6212], center = false);
    translate(v = [0, 0, 23.4212]) {
      cube(size = [7.8, 5.2, 10.4], center = false);
    }
    translate(v = [13, 0, 23.4212]) {
      cube(size = [7.8, 5.2, 10.4], center = false);
    }
    translate(v = [26, 0, 23.4212]) {
      cube(size = [7.8, 5.2, 10.4], center = false);
    }
  }
}

translate(v = [5.74592, -62.6744, 0]) {
  difference() {
    union() {
      difference() {
        cylinder(h = 41.7594, r1 = 8.98379, r2 = 7.18703, $fa = 45);
        translate(v = [0, 0, -12.5278]) {
          cylinder(h = 33.4075, r1 = 6.28865, r2 = 5.03092, $fa = 45);
        }
        translate(v = [0, 0, 18.7917]) {
          cylinder(h = 14.6158, r1 = 5.39027, r2 = 0.718703, $fa = 45);
        }
      }
      translate(v = [0, 0, 36.3692]) {
        cylinder(h = 5.39027, r1 = 7.27687, r2 = 10.7805, $fa = 45);
        translate(v = [0, 0, 5.39027]) {
          cylinder(h = 5.39027, r1 = 10.7805, r2 = 10.7805, $fa = 45);
          translate(v = [0, 0, 5.39027]) {
            cylinder(h = 13.5011, r1 = 10.7805, r2 = 0, $fa = 45);
            cylinder(h = 27.0022, r1 = 5.39027, r2 = 0, $fa = 45);
          }
        }
      }
    }
    translate(v = [0, 0, 44.7594]) {
      rotate(a = 22.5, v = [0, 0, 1]) {
        union() {
          scale(v = [0.1, 0.1, 0.1]) {
            translate(v = [-13.4757, -269.514, -18.866]) {
              union() {
                cube(size = [26.9514, 539.027, 24.2562], center = false);
                translate(v = [13.4757, 539.027, 24.2562]) {
                  rotate(a = 90, v = [1, 0, 0]) {
                    cylinder(h = 539.027, r1 = 13.4757, r2 = 13.4757);
                  }
                }
              }
            }
          }
          rotate(a = 90, v = [0, 0, 1]) {
            scale(v = [0.1, 0.1, 0.1]) {
              translate(v = [-13.4757, -269.514, -18.866]) {
                union() {
                  cube(size = [26.9514, 539.027, 24.2562], center = false);
                  translate(v = [13.4757, 539.027, 24.2562]) {
                    rotate(a = 90, v = [1, 0, 0]) {
                      cylinder(h = 539.027, r1 = 13.4757, r2 = 13.4757);
                    }
                  }
                }
              }
            }
          }
          rotate(a = 45, v = [0, 0, 1]) {
            scale(v = [0.1, 0.1, 0.1]) {
              translate(v = [-13.4757, -269.514, -18.866]) {
                union() {
                  cube(size = [26.9514, 539.027, 24.2562], center = false);
                  translate(v = [13.4757, 539.027, 24.2562]) {
                    rotate(a = 90, v = [1, 0, 0]) {
                      cylinder(h = 539.027, r1 = 13.4757, r2 = 13.4757);
                    }
                  }
                }
              }
            }
          }
          rotate(a = 135, v = [0, 0, 1]) {
            scale(v = [0.1, 0.1, 0.1]) {
              translate(v = [-13.4757, -269.514, -18.866]) {
                union() {
                  cube(size = [26.9514, 539.027, 24.2562], center = false);
                  translate(v = [13.4757, 539.027, 24.2562]) {
                    rotate(a = 90, v = [1, 0, 0]) {
                      cylinder(h = 539.027, r1 = 13.4757, r2 = 13.4757);
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

/* 44.259567, 19.629851, angle = 66 */
translate(v = [39.3696, -53.9727, 0]) {
  rotate(a = 66.0819, v = [0, 0, 1]) {
    cube(size = [48.4174, 5.2, 28.2637], center = false);
    translate(v = [0, 0, 23.0637]) {
      cube(size = [7.8, 5.2, 10.4], center = false);
    }
    translate(v = [13, 0, 23.0637]) {
      cube(size = [7.8, 5.2, 10.4], center = false);
    }
    translate(v = [26, 0, 23.0637]) {
      cube(size = [7.8, 5.2, 10.4], center = false);
    }
    translate(v = [39, 0, 23.0637]) {
      cube(size = [7.8, 5.2, 10.4], center = false);
    }
  }
}

translate(v = [39.3696, -53.9727, 0]) {
  difference() {
    union() {
      difference() {
        cylinder(h = 38.3835, r1 = 12.2723, r2 = 9.81786, $fa = 45);
        translate(v = [0, 0, -11.515]) {
          cylinder(h = 30.7068, r1 = 8.59063, r2 = 6.8725, $fa = 45);
        }
        translate(v = [0, 0, 17.2726]) {
          cylinder(h = 13.4342, r1 = 7.3634, r2 = 0.981786, $fa = 45);
        }
      }
      translate(v = [0, 0, 31.0201]) {
        cylinder(h = 7.3634, r1 = 9.94059, r2 = 14.7268, $fa = 45);
        translate(v = [0, 0, 7.3634]) {
          cylinder(h = 7.3634, r1 = 14.7268, r2 = 14.7268, $fa = 45);
          translate(v = [0, 0, 7.3634]) {
            cylinder(h = 17.9236, r1 = 14.7268, r2 = 0, $fa = 45);
          }
        }
      }
    }
    translate(v = [0, 0, 41.3835]) {
      rotate(a = 22.5, v = [0, 0, 1]) {
        union() {
          scale(v = [0.1, 0.1, 0.1]) {
            translate(v = [-18.4085, -368.17, -25.7719]) {
              union() {
                cube(size = [36.817, 736.34, 33.1353], center = false);
                translate(v = [18.4085, 736.34, 33.1353]) {
                  rotate(a = 90, v = [1, 0, 0]) {
                    cylinder(h = 736.34, r1 = 18.4085, r2 = 18.4085);
                  }
                }
              }
            }
          }
          rotate(a = 90, v = [0, 0, 1]) {
            scale(v = [0.1, 0.1, 0.1]) {
              translate(v = [-18.4085, -368.17, -25.7719]) {
                union() {
                  cube(size = [36.817, 736.34, 33.1353], center = false);
                  translate(v = [18.4085, 736.34, 33.1353]) {
                    rotate(a = 90, v = [1, 0, 0]) {
                      cylinder(h = 736.34, r1 = 18.4085, r2 = 18.4085);
                    }
                  }
                }
              }
            }
          }
          rotate(a = 45, v = [0, 0, 1]) {
            scale(v = [0.1, 0.1, 0.1]) {
              translate(v = [-18.4085, -368.17, -25.7719]) {
                union() {
                  cube(size = [36.817, 736.34, 33.1353], center = false);
                  translate(v = [18.4085, 736.34, 33.1353]) {
                    rotate(a = 90, v = [1, 0, 0]) {
                      cylinder(h = 736.34, r1 = 18.4085, r2 = 18.4085);
                    }
                  }
                }
              }
            }
          }
          rotate(a = 135, v = [0, 0, 1]) {
            scale(v = [0.1, 0.1, 0.1]) {
              translate(v = [-18.4085, -368.17, -25.7719]) {
                union() {
                  cube(size = [36.817, 736.34, 33.1353], center = false);
                  translate(v = [18.4085, 736.34, 33.1353]) {
                    rotate(a = 90, v = [1, 0, 0]) {
                      cylinder(h = 736.34, r1 = 18.4085, r2 = 18.4085);
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

/* 27.776820, -28.431230, angle = 135 */
translate(v = [48.597, 6.47007, 0]) {
  rotate(a = 135.667, v = [0, 0, 1]) {
    cube(size = [39.7478, 3.04, 46.5624], center = false);
    translate(v = [0, 0, 43.5224]) {
      cube(size = [4.56, 3.04, 6.08], center = false);
    }
    translate(v = [7.6, 0, 43.5224]) {
      cube(size = [4.56, 3.04, 6.08], center = false);
    }
    translate(v = [15.2, 0, 43.5224]) {
      cube(size = [4.56, 3.04, 6.08], center = false);
    }
    translate(v = [22.8, 0, 43.5224]) {
      cube(size = [4.56, 3.04, 6.08], center = false);
    }
    translate(v = [30.4, 0, 43.5224]) {
      cube(size = [4.56, 3.04, 6.08], center = false);
    }
    translate(v = [38, 0, 43.5224]) {
      cube(size = [4.56, 3.04, 6.08], center = false);
    }
  }
}

translate(v = [48.597, 6.47007, 0]) {
  union() {
    cylinder(h = 43.933, r1 = 0.882544, r2 = 0.882544, $fa = 5);
    translate(v = [0, 0, 29.2886]) {
      cylinder(h = 14.6443, r1 = 0.588363, r2 = 7.06035, $fa = 5);
      translate(v = [0, 0, 14.6443]) {
        difference() {
          cylinder(h = 9.4138, r1 = 7.06035, r2 = 7.06035, $fa = 5);
          translate(v = [0, 0, 4.7069]) {
            cylinder(h = 5.88363, r1 = 5.88363, r2 = 5.88363, $fa = 5);
            translate(v = [0, 0, 4.11854]) {
              rotate(a = 22.5, v = [0, 0, 1]) {
                union() {
                  scale(v = [0.1, 0.1, 0.1]) {
                    translate(v = [-8.82544, -176.509, -23.6816]) {
                      union() {
                        cube(size = [17.6509, 353.018, 38.5377], center = false);
                        translate(v = [8.82544, 353.018, 38.5377]) {
                          rotate(a = 90, v = [1, 0, 0]) {
                            cylinder(h = 353.018, r1 = 8.82544, r2 = 8.82544);
                          }
                        }
                      }
                    }
                  }
                  rotate(a = 90, v = [0, 0, 1]) {
                    scale(v = [0.1, 0.1, 0.1]) {
                      translate(v = [-8.82544, -176.509, -23.6816]) {
                        union() {
                          cube(size = [17.6509, 353.018, 38.5377], center = false);
                          translate(v = [8.82544, 353.018, 38.5377]) {
                            rotate(a = 90, v = [1, 0, 0]) {
                              cylinder(h = 353.018, r1 = 8.82544, r2 = 8.82544);
                            }
                          }
                        }
                      }
                    }
                  }
                  rotate(a = 45, v = [0, 0, 1]) {
                    scale(v = [0.1, 0.1, 0.1]) {
                      translate(v = [-8.82544, -176.509, -23.6816]) {
                        union() {
                          cube(size = [17.6509, 353.018, 38.5377], center = false);
                          translate(v = [8.82544, 353.018, 38.5377]) {
                            rotate(a = 90, v = [1, 0, 0]) {
                              cylinder(h = 353.018, r1 = 8.82544, r2 = 8.82544);
                            }
                          }
                        }
                      }
                    }
                  }
                  rotate(a = 135, v = [0, 0, 1]) {
                    scale(v = [0.1, 0.1, 0.1]) {
                      translate(v = [-8.82544, -176.509, -23.6816]) {
                        union() {
                          cube(size = [17.6509, 353.018, 38.5377], center = false);
                          translate(v = [8.82544, 353.018, 38.5377]) {
                            rotate(a = 90, v = [1, 0, 0]) {
                              cylinder(h = 353.018, r1 = 8.82544, r2 = 8.82544);
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

/* 0.951307, -40.439405, angle = 178 */
translate(v = [20.1658, 34.2469, 0]) {
  rotate(a = 178.652, v = [0, 0, 1]) {
    cube(size = [40.4506, 3.04, 46.994], center = false);
    translate(v = [0, 0, 43.954]) {
      cube(size = [4.56, 3.04, 6.08], center = false);
    }
    translate(v = [7.6, 0, 43.954]) {
      cube(size = [4.56, 3.04, 6.08], center = false);
    }
    translate(v = [15.2, 0, 43.954]) {
      cube(size = [4.56, 3.04, 6.08], center = false);
    }
    translate(v = [22.8, 0, 43.954]) {
      cube(size = [4.56, 3.04, 6.08], center = false);
    }
    translate(v = [30.4, 0, 43.954]) {
      cube(size = [4.56, 3.04, 6.08], center = false);
    }
    translate(v = [38, 0, 43.954]) {
      cube(size = [4.56, 3.04, 6.08], center = false);
    }
  }
}

translate(v = [20.1658, 34.2469, 0]) {
  difference() {
    union() {
      difference() {
        cylinder(h = 57.8388, r1 = 7.09473, r2 = 5.67579, $fa = 60);
        translate(v = [0, 0, -17.3516]) {
          cylinder(h = 46.2711, r1 = 4.96631, r2 = 3.97305, $fa = 60);
        }
        translate(v = [0, 0, 26.0275]) {
          cylinder(h = 20.2436, r1 = 4.25684, r2 = 0.567579, $fa = 60);
        }
      }
      translate(v = [0, 0, 53.582]) {
        cylinder(h = 4.25684, r1 = 5.74673, r2 = 8.51368, $fa = 60);
        translate(v = [0, 0, 4.25684]) {
          difference() {
            cylinder(h = 4.25684, r1 = 8.51368, r2 = 8.51368, $fa = 60);
            cylinder(h = 5.32105, r1 = 7.09473, r2 = 7.09473, $fa = 60);
            translate(v = [0, 0, 4.96631]) {
              rotate(a = 22.5, v = [0, 0, 1]) {
                union() {
                  scale(v = [0.1, 0.1, 0.1]) {
                    translate(v = [-10.6421, -212.842, -28.5563]) {
                      union() {
                        cube(size = [21.2842, 425.684, 46.4705], center = false);
                        translate(v = [10.6421, 425.684, 46.4705]) {
                          rotate(a = 90, v = [1, 0, 0]) {
                            cylinder(h = 425.684, r1 = 10.6421, r2 = 10.6421);
                          }
                        }
                      }
                    }
                  }
                  rotate(a = 90, v = [0, 0, 1]) {
                    scale(v = [0.1, 0.1, 0.1]) {
                      translate(v = [-10.6421, -212.842, -28.5563]) {
                        union() {
                          cube(size = [21.2842, 425.684, 46.4705], center = false);
                          translate(v = [10.6421, 425.684, 46.4705]) {
                            rotate(a = 90, v = [1, 0, 0]) {
                              cylinder(h = 425.684, r1 = 10.6421, r2 = 10.6421);
                            }
                          }
                        }
                      }
                    }
                  }
                  rotate(a = 45, v = [0, 0, 1]) {
                    scale(v = [0.1, 0.1, 0.1]) {
                      translate(v = [-10.6421, -212.842, -28.5563]) {
                        union() {
                          cube(size = [21.2842, 425.684, 46.4705], center = false);
                          translate(v = [10.6421, 425.684, 46.4705]) {
                            rotate(a = 90, v = [1, 0, 0]) {
                              cylinder(h = 425.684, r1 = 10.6421, r2 = 10.6421);
                            }
                          }
                        }
                      }
                    }
                  }
                  rotate(a = 135, v = [0, 0, 1]) {
                    scale(v = [0.1, 0.1, 0.1]) {
                      translate(v = [-10.6421, -212.842, -28.5563]) {
                        union() {
                          cube(size = [21.2842, 425.684, 46.4705], center = false);
                          translate(v = [10.6421, 425.684, 46.4705]) {
                            rotate(a = 90, v = [1, 0, 0]) {
                              cylinder(h = 425.684, r1 = 10.6421, r2 = 10.6421);
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    translate(v = [0, 0, 34.0717]) {
      rotate(a = 311.82, v = [0, 0, 1]) {
        scale(v = [0.1, 0.1, 0.1]) {
          translate(v = [-10, -106.421, -25]) {
            union() {
              cube(size = [20, 212.842, 40], center = false);
              translate(v = [10, 212.842, 40]) {
                rotate(a = 90, v = [1, 0, 0]) {
                  cylinder(h = 212.842, r1 = 10, r2 = 10);
                }
              }
            }
          }
        }
      }
    }
  }
}

/* -31.338223, -19.155272, angle = -121 */
translate(v = [-20.2736, 35.1982, 0]) {
  rotate(a = -121.435, v = [0, 0, 1]) {
    cube(size = [36.7289, 3.04, 43.9504], center = false);
    translate(v = [0, 0, 40.9104]) {
      cube(size = [4.56, 3.04, 6.08], center = false);
    }
    translate(v = [7.6, 0, 40.9104]) {
      cube(size = [4.56, 3.04, 6.08], center = false);
    }
    translate(v = [15.2, 0, 40.9104]) {
      cube(size = [4.56, 3.04, 6.08], center = false);
    }
    translate(v = [22.8, 0, 40.9104]) {
      cube(size = [4.56, 3.04, 6.08], center = false);
    }
    translate(v = [30.4, 0, 40.9104]) {
      cube(size = [4.56, 3.04, 6.08], center = false);
    }
  }
}

translate(v = [-20.2736, 35.1982, 0]) {
  union() {
    cylinder(h = 47.1044, r1 = 0.697442, r2 = 0.697442, $fa = 5);
    translate(v = [0, 0, 31.403]) {
      cylinder(h = 15.7015, r1 = 0.464961, r2 = 5.57954, $fa = 5);
      translate(v = [0, 0, 15.7015]) {
        difference() {
          cylinder(h = 7.43938, r1 = 5.57954, r2 = 5.57954, $fa = 5);
          translate(v = [0, 0, 3.71969]) {
            cylinder(h = 4.64961, r1 = 4.64961, r2 = 4.64961, $fa = 5);
            translate(v = [0, 0, 3.25473]) {
              rotate(a = 22.5, v = [0, 0, 1]) {
                union() {
                  scale(v = [0.1, 0.1, 0.1]) {
                    translate(v = [-6.97442, -139.488, -18.7147]) {
                      union() {
                        cube(size = [13.9488, 278.977, 30.455], center = false);
                        translate(v = [6.97442, 278.977, 30.455]) {
                          rotate(a = 90, v = [1, 0, 0]) {
                            cylinder(h = 278.977, r1 = 6.97442, r2 = 6.97442);
                          }
                        }
                      }
                    }
                  }
                  rotate(a = 90, v = [0, 0, 1]) {
                    scale(v = [0.1, 0.1, 0.1]) {
                      translate(v = [-6.97442, -139.488, -18.7147]) {
                        union() {
                          cube(size = [13.9488, 278.977, 30.455], center = false);
                          translate(v = [6.97442, 278.977, 30.455]) {
                            rotate(a = 90, v = [1, 0, 0]) {
                              cylinder(h = 278.977, r1 = 6.97442, r2 = 6.97442);
                            }
                          }
                        }
                      }
                    }
                  }
                  rotate(a = 45, v = [0, 0, 1]) {
                    scale(v = [0.1, 0.1, 0.1]) {
                      translate(v = [-6.97442, -139.488, -18.7147]) {
                        union() {
                          cube(size = [13.9488, 278.977, 30.455], center = false);
                          translate(v = [6.97442, 278.977, 30.455]) {
                            rotate(a = 90, v = [1, 0, 0]) {
                              cylinder(h = 278.977, r1 = 6.97442, r2 = 6.97442);
                            }
                          }
                        }
                      }
                    }
                  }
                  rotate(a = 135, v = [0, 0, 1]) {
                    scale(v = [0.1, 0.1, 0.1]) {
                      translate(v = [-6.97442, -139.488, -18.7147]) {
                        union() {
                          cube(size = [13.9488, 278.977, 30.455], center = false);
                          translate(v = [6.97442, 278.977, 30.455]) {
                            rotate(a = 90, v = [1, 0, 0]) {
                              cylinder(h = 278.977, r1 = 6.97442, r2 = 6.97442);
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

/* -32.328170, 23.958782, angle = -53 */
translate(v = [-39.4289, 3.85998, 0]) {
  rotate(a = -53.4574, v = [0, 0, 1]) {
    cube(size = [40.2385, 3.04, 48.0131], center = false);
    translate(v = [0, 0, 44.9731]) {
      cube(size = [4.56, 3.04, 6.08], center = false);
    }
    translate(v = [7.6, 0, 44.9731]) {
      cube(size = [4.56, 3.04, 6.08], center = false);
    }
    translate(v = [15.2, 0, 44.9731]) {
      cube(size = [4.56, 3.04, 6.08], center = false);
    }
    translate(v = [22.8, 0, 44.9731]) {
      cube(size = [4.56, 3.04, 6.08], center = false);
    }
    translate(v = [30.4, 0, 44.9731]) {
      cube(size = [4.56, 3.04, 6.08], center = false);
    }
    translate(v = [38, 0, 44.9731]) {
      cube(size = [4.56, 3.04, 6.08], center = false);
    }
  }
}

translate(v = [-39.4289, 3.85998, 0]) {
  difference() {
    union() {
      difference() {
        cylinder(h = 64.0369, r1 = 7.16597, r2 = 5.73278, $fa = 5);
        translate(v = [0, 0, -19.2111]) {
          cylinder(h = 51.2295, r1 = 5.01618, r2 = 4.01294, $fa = 5);
        }
        translate(v = [0, 0, 28.8166]) {
          cylinder(h = 22.4129, r1 = 4.29958, r2 = 0.573278, $fa = 5);
        }
      }
      translate(v = [0, 0, 59.7373]) {
        cylinder(h = 4.29958, r1 = 5.80444, r2 = 8.59916, $fa = 5);
        translate(v = [0, 0, 4.29958]) {
          difference() {
            cylinder(h = 4.29958, r1 = 8.59916, r2 = 8.59916, $fa = 5);
            cylinder(h = 5.37448, r1 = 7.16597, r2 = 7.16597, $fa = 5);
            translate(v = [0, 0, 5.01618]) {
              rotate(a = 22.5, v = [0, 0, 1]) {
                union() {
                  scale(v = [0.1, 0.1, 0.1]) {
                    translate(v = [-10.749, -214.979, -28.843]) {
                      union() {
                        cube(size = [21.4979, 429.958, 46.9371], center = false);
                        translate(v = [10.749, 429.958, 46.9371]) {
                          rotate(a = 90, v = [1, 0, 0]) {
                            cylinder(h = 429.958, r1 = 10.749, r2 = 10.749);
                          }
                        }
                      }
                    }
                  }
                  rotate(a = 90, v = [0, 0, 1]) {
                    scale(v = [0.1, 0.1, 0.1]) {
                      translate(v = [-10.749, -214.979, -28.843]) {
                        union() {
                          cube(size = [21.4979, 429.958, 46.9371], center = false);
                          translate(v = [10.749, 429.958, 46.9371]) {
                            rotate(a = 90, v = [1, 0, 0]) {
                              cylinder(h = 429.958, r1 = 10.749, r2 = 10.749);
                            }
                          }
                        }
                      }
                    }
                  }
                  rotate(a = 45, v = [0, 0, 1]) {
                    scale(v = [0.1, 0.1, 0.1]) {
                      translate(v = [-10.749, -214.979, -28.843]) {
                        union() {
                          cube(size = [21.4979, 429.958, 46.9371], center = false);
                          translate(v = [10.749, 429.958, 46.9371]) {
                            rotate(a = 90, v = [1, 0, 0]) {
                              cylinder(h = 429.958, r1 = 10.749, r2 = 10.749);
                            }
                          }
                        }
                      }
                    }
                  }
                  rotate(a = 135, v = [0, 0, 1]) {
                    scale(v = [0.1, 0.1, 0.1]) {
                      translate(v = [-10.749, -214.979, -28.843]) {
                        union() {
                          cube(size = [21.4979, 429.958, 46.9371], center = false);
                          translate(v = [10.749, 429.958, 46.9371]) {
                            rotate(a = 90, v = [1, 0, 0]) {
                              cylinder(h = 429.958, r1 = 10.749, r2 = 10.749);
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    translate(v = [0, 0, 39.6162]) {
      rotate(a = 92.9782, v = [0, 0, 1]) {
        scale(v = [0.1, 0.1, 0.1]) {
          translate(v = [-10, -107.49, -25]) {
            union() {
              cube(size = [20, 214.979, 40], center = false);
              translate(v = [10, 214.979, 40]) {
                rotate(a = 90, v = [1, 0, 0]) {
                  cylinder(h = 214.979, r1 = 10, r2 = 10);
                }
              }
            }
          }
        }
      }
    }
    translate(v = [0, 0, 40.8163]) {
      rotate(a = 205.233, v = [0, 0, 1]) {
        scale(v = [0.1, 0.1, 0.1]) {
          translate(v = [-10, -107.49, -25]) {
            union() {
              cube(size = [20, 214.979, 40], center = false);
              translate(v = [10, 214.979, 40]) {
                rotate(a = 90, v = [1, 0, 0]) {
                  cylinder(h = 214.979, r1 = 10, r2 = 10);
                }
              }
            }
          }
        }
      }
    }
  }
}

/* -2.039661, 36.677982, angle = -3 */
translate(v = [-15.4701, -28.4682, 0]) {
  rotate(a = -3.18294, v = [0, 0, 1]) {
    cube(size = [36.7347, 3.04, 48.3177], center = false);
    translate(v = [0, 0, 45.2777]) {
      cube(size = [4.56, 3.04, 6.08], center = false);
    }
    translate(v = [7.6, 0, 45.2777]) {
      cube(size = [4.56, 3.04, 6.08], center = false);
    }
    translate(v = [15.2, 0, 45.2777]) {
      cube(size = [4.56, 3.04, 6.08], center = false);
    }
    translate(v = [22.8, 0, 45.2777]) {
      cube(size = [4.56, 3.04, 6.08], center = false);
    }
    translate(v = [30.4, 0, 45.2777]) {
      cube(size = [4.56, 3.04, 6.08], center = false);
    }
  }
}

translate(v = [-15.4701, -28.4682, 0]) {
  difference() {
    union() {
      difference() {
        cylinder(h = 61.0633, r1 = 7.30551, r2 = 5.84441, $fa = 5);
        translate(v = [0, 0, -18.319]) {
          cylinder(h = 48.8506, r1 = 5.11386, r2 = 4.09109, $fa = 5);
        }
        translate(v = [0, 0, 27.4785]) {
          cylinder(h = 21.3722, r1 = 4.38331, r2 = 0.584441, $fa = 5);
        }
      }
      translate(v = [0, 0, 56.68]) {
        cylinder(h = 4.38331, r1 = 5.91746, r2 = 8.76661, $fa = 5);
        translate(v = [0, 0, 4.38331]) {
          difference() {
            cylinder(h = 4.38331, r1 = 8.76661, r2 = 8.76661, $fa = 5);
            cylinder(h = 5.47913, r1 = 7.30551, r2 = 7.30551, $fa = 5);
            translate(v = [0, 0, 5.11386]) {
              rotate(a = 22.5, v = [0, 0, 1]) {
                union() {
                  scale(v = [0.1, 0.1, 0.1]) {
                    translate(v = [-10.9583, -219.165, -29.4047]) {
                      union() {
                        cube(size = [21.9165, 438.331, 47.8511], center = false);
                        translate(v = [10.9583, 438.331, 47.8511]) {
                          rotate(a = 90, v = [1, 0, 0]) {
                            cylinder(h = 438.331, r1 = 10.9583, r2 = 10.9583);
                          }
                        }
                      }
                    }
                  }
                  rotate(a = 90, v = [0, 0, 1]) {
                    scale(v = [0.1, 0.1, 0.1]) {
                      translate(v = [-10.9583, -219.165, -29.4047]) {
                        union() {
                          cube(size = [21.9165, 438.331, 47.8511], center = false);
                          translate(v = [10.9583, 438.331, 47.8511]) {
                            rotate(a = 90, v = [1, 0, 0]) {
                              cylinder(h = 438.331, r1 = 10.9583, r2 = 10.9583);
                            }
                          }
                        }
                      }
                    }
                  }
                  rotate(a = 45, v = [0, 0, 1]) {
                    scale(v = [0.1, 0.1, 0.1]) {
                      translate(v = [-10.9583, -219.165, -29.4047]) {
                        union() {
                          cube(size = [21.9165, 438.331, 47.8511], center = false);
                          translate(v = [10.9583, 438.331, 47.8511]) {
                            rotate(a = 90, v = [1, 0, 0]) {
                              cylinder(h = 438.331, r1 = 10.9583, r2 = 10.9583);
                            }
                          }
                        }
                      }
                    }
                  }
                  rotate(a = 135, v = [0, 0, 1]) {
                    scale(v = [0.1, 0.1, 0.1]) {
                      translate(v = [-10.9583, -219.165, -29.4047]) {
                        union() {
                          cube(size = [21.9165, 438.331, 47.8511], center = false);
                          translate(v = [10.9583, 438.331, 47.8511]) {
                            rotate(a = 90, v = [1, 0, 0]) {
                              cylinder(h = 438.331, r1 = 10.9583, r2 = 10.9583);
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    translate(v = [0, 0, 44.8369]) {
      rotate(a = 198.63, v = [0, 0, 1]) {
        scale(v = [0.1, 0.1, 0.1]) {
          translate(v = [-10, -109.583, -25]) {
            union() {
              cube(size = [20, 219.165, 40], center = false);
              translate(v = [10, 219.165, 40]) {
                rotate(a = 90, v = [1, 0, 0]) {
                  cylinder(h = 219.165, r1 = 10, r2 = 10);
                }
              }
            }
          }
        }
      }
    }
    translate(v = [0, 0, 39.2729]) {
      rotate(a = 289.167, v = [0, 0, 1]) {
        scale(v = [0.1, 0.1, 0.1]) {
          translate(v = [-10, -109.583, -25]) {
            union() {
              cube(size = [20, 219.165, 40], center = false);
              translate(v = [10, 219.165, 40]) {
                rotate(a = 90, v = [1, 0, 0]) {
                  cylinder(h = 219.165, r1 = 10, r2 = 10);
                }
              }
            }
          }
        }
      }
    }
  }
}

/* 36.977928, 27.389144, angle = 53 */
translate(v = [21.2079, -30.5079, 0]) {
  rotate(a = 53.4731, v = [0, 0, 1]) {
    cube(size = [46.0167, 3.04, 48.17], center = false);
    translate(v = [0, 0, 45.13]) {
      cube(size = [4.56, 3.04, 6.08], center = false);
    }
    translate(v = [7.6, 0, 45.13]) {
      cube(size = [4.56, 3.04, 6.08], center = false);
    }
    translate(v = [15.2, 0, 45.13]) {
      cube(size = [4.56, 3.04, 6.08], center = false);
    }
    translate(v = [22.8, 0, 45.13]) {
      cube(size = [4.56, 3.04, 6.08], center = false);
    }
    translate(v = [30.4, 0, 45.13]) {
      cube(size = [4.56, 3.04, 6.08], center = false);
    }
    translate(v = [38, 0, 45.13]) {
      cube(size = [4.56, 3.04, 6.08], center = false);
    }
    translate(v = [45.6, 0, 45.13]) {
      cube(size = [4.56, 3.04, 6.08], center = false);
    }
  }
}

translate(v = [21.2079, -30.5079, 0]) {
  difference() {
    union() {
      difference() {
        cylinder(h = 64.8213, r1 = 6.36835, r2 = 5.09468, $fa = 45);
        translate(v = [0, 0, -19.4464]) {
          cylinder(h = 51.857, r1 = 4.45785, r2 = 3.56628, $fa = 45);
        }
        translate(v = [0, 0, 29.1696]) {
          cylinder(h = 22.6874, r1 = 3.82101, r2 = 0.509468, $fa = 45);
        }
      }
      translate(v = [0, 0, 61.0003]) {
        cylinder(h = 3.82101, r1 = 5.15837, r2 = 7.64202, $fa = 45);
        translate(v = [0, 0, 3.82101]) {
          difference() {
            cylinder(h = 3.82101, r1 = 7.64202, r2 = 7.64202, $fa = 45);
            cylinder(h = 4.77626, r1 = 6.36835, r2 = 6.36835, $fa = 45);
            translate(v = [0, 0, 4.45785]) {
              rotate(a = 22.5, v = [0, 0, 1]) {
                union() {
                  scale(v = [0.1, 0.1, 0.1]) {
                    translate(v = [-9.55253, -191.051, -25.6326]) {
                      union() {
                        cube(size = [19.1051, 382.101, 41.7127], center = false);
                        translate(v = [9.55253, 382.101, 41.7127]) {
                          rotate(a = 90, v = [1, 0, 0]) {
                            cylinder(h = 382.101, r1 = 9.55253, r2 = 9.55253);
                          }
                        }
                      }
                    }
                  }
                  rotate(a = 90, v = [0, 0, 1]) {
                    scale(v = [0.1, 0.1, 0.1]) {
                      translate(v = [-9.55253, -191.051, -25.6326]) {
                        union() {
                          cube(size = [19.1051, 382.101, 41.7127], center = false);
                          translate(v = [9.55253, 382.101, 41.7127]) {
                            rotate(a = 90, v = [1, 0, 0]) {
                              cylinder(h = 382.101, r1 = 9.55253, r2 = 9.55253);
                            }
                          }
                        }
                      }
                    }
                  }
                  rotate(a = 45, v = [0, 0, 1]) {
                    scale(v = [0.1, 0.1, 0.1]) {
                      translate(v = [-9.55253, -191.051, -25.6326]) {
                        union() {
                          cube(size = [19.1051, 382.101, 41.7127], center = false);
                          translate(v = [9.55253, 382.101, 41.7127]) {
                            rotate(a = 90, v = [1, 0, 0]) {
                              cylinder(h = 382.101, r1 = 9.55253, r2 = 9.55253);
                            }
                          }
                        }
                      }
                    }
                  }
                  rotate(a = 135, v = [0, 0, 1]) {
                    scale(v = [0.1, 0.1, 0.1]) {
                      translate(v = [-9.55253, -191.051, -25.6326]) {
                        union() {
                          cube(size = [19.1051, 382.101, 41.7127], center = false);
                          translate(v = [9.55253, 382.101, 41.7127]) {
                            rotate(a = 90, v = [1, 0, 0]) {
                              cylinder(h = 382.101, r1 = 9.55253, r2 = 9.55253);
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    translate(v = [0, 0, 43.031]) {
      rotate(a = 289.618, v = [0, 0, 1]) {
        scale(v = [0.1, 0.1, 0.1]) {
          translate(v = [-10, -95.5253, -25]) {
            union() {
              cube(size = [20, 191.051, 40], center = false);
              translate(v = [10, 191.051, 40]) {
                rotate(a = 90, v = [1, 0, 0]) {
                  cylinder(h = 191.051, r1 = 10, r2 = 10);
                }
              }
            }
          }
        }
      }
    }
    translate(v = [0, 0, 44.3269]) {
      rotate(a = 197.21, v = [0, 0, 1]) {
        scale(v = [0.1, 0.1, 0.1]) {
          translate(v = [-10, -95.5253, -25]) {
            union() {
              cube(size = [20, 191.051, 40], center = false);
              translate(v = [10, 191.051, 40]) {
                rotate(a = 90, v = [1, 0, 0]) {
                  cylinder(h = 191.051, r1 = 10, r2 = 10);
                }
              }
            }
          }
        }
      }
    }
  }
}

}

}

